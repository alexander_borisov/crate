FROM node:current-alpine
RUN addgroup -S app && adduser -SDH -g "" -G app app
COPY code /code
RUN cd /code/api && npm run setup && cd /code/web && npm run setup
RUN mkdir /code/web/public/js/bundles && chown -R app:app /code/web/public/js/bundles
RUN chown -R app:app /code
USER app
WORKDIR /code/web/
EXPOSE 3000
ENTRYPOINT ["npm", "start"]
